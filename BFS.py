graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary

    BFS("A", graph_dictionary)


def BFS(selecting_node, graph_dictionary):
    queue = []
    visited_list = []
    # add last
    queue.append(selecting_node)
    # add last
    visited_list.append(selecting_node)

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if node not in visited_list:
                # add last
                queue.append(node)
                # add last
                visited_list.append(node)

        print(dequeue_node)


main()
