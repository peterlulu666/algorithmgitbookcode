# graph = {"a": {"b": 10, "c": 3},
#          "b": {"c": 1, "d": 2},
#          "c": {"b": 4, "d": 8, "e": 2},
#          "d": {"e": 7},
#          "e": {"d": 9}}


graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def dijkstra(selecting_node):
    shortest_distance = {}
    # let all nodes' distance to be infinity
    for node in graph:
        shortest_distance[node] = infinity
    # let the selecting node's distance to be 0
    shortest_distance[selecting_node] = 0

    done_list = []

    # while not all the node have been marked as done
    while sorted(done_list) != sorted(list(graph)):
        # find the node with the smallest distance
        # find the key with minimum value
        compare_distance = {}
        for node in graph:
            if node not in done_list:
                compare_distance[node] = shortest_distance[node]
        min_node = min(compare_distance, key=compare_distance.get)
        # min_node = [key for key in compare_distance if compare_distance[key] == min(compare_distance.values())][0]

        # compare min_node + weight with child_node's distance
        # if shortest_distance[min_node] + weight is smaller,
        # update child_node's distance
        adjacent_node_dict = graph[min_node]
        for child_node, weight in adjacent_node_dict.items():
            shortest_distance[child_node] = min(shortest_distance[min_node] + weight, shortest_distance[child_node])
            # if shortest_distance[min_node] + weight < shortest_distance[child_node]:
            #     shortest_distance[child_node] = shortest_distance[min_node] + weight

        # mark the min_node as done
        done_list.append(min_node)

    print(shortest_distance)


dijkstra("a")
