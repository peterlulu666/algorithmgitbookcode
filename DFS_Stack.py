graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary
    DFS_Stack("A", graph_dictionary)


def DFS_Stack(selecting_node, graph_dictionary):
    stack = []
    visited_list = []
    # add first
    stack.insert(0, selecting_node)
    # add last
    visited_list.append(selecting_node)

    while len(stack) != 0:
        # remove first
        popped_node = stack.pop(0)

        adjacent_nodes_list = graph_dictionary[popped_node]

        for node in adjacent_nodes_list:
            if node not in visited_list:
                # add fist
                stack.insert(0, node)
                # add last
                visited_list.append(node)
        print(popped_node)


main()
