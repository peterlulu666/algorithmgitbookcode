graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}

# Initialize
color_dictionary = {}
for key in graph_dictionary.keys():
    color_dictionary[key] = "white"

parent_dictionary = {}
for key in graph_dictionary.keys():
    parent_dictionary[key] = None

# count the timestamps
time = 0

discovery_time_dictionary = {}
for key in graph_dictionary.keys():
    discovery_time_dictionary[key] = -1

finishing_time_dictionary = {}
for key in graph_dictionary.keys():
    finishing_time_dictionary[key] = -1

DFS_print = []
topological_order_print = []


def main():
    DFS("A")

    print(DFS_print)
    print(topological_order_print)

    for key in graph_dictionary.keys():
        print(str(key) + ": " +
              str(discovery_time_dictionary[key]) +
              ", " +
              str(finishing_time_dictionary[key]))


def DFS(selecting_node):
    global color_dictionary
    global time
    global discovery_time_dictionary
    global graph_dictionary
    global parent_dictionary
    global finishing_time_dictionary
    global DFS_print
    global topological_order_print

    color_dictionary[selecting_node] = "grey"
    DFS_print.append(selecting_node)
    time = time + 1
    discovery_time_dictionary[selecting_node] = time

    adjacent_nodes_list = graph_dictionary[selecting_node]

    for node in adjacent_nodes_list:
        if color_dictionary[node] == "white":
            parent_dictionary[node] = selecting_node
            DFS(node)

    color_dictionary[selecting_node] = "black"
    topological_order_print.append(selecting_node)
    time = time + 1
    finishing_time_dictionary[selecting_node] = time


main()
