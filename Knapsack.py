# 0 1 knapsack
def knapsack(value_list, weight_list, number_of_item, capacity):
    # declare 2 D array
    # capacity is the column
    # number_of_item is the row
    dp = [[-1] * (capacity + 1) for x in range(number_of_item + 1)]

    # initialize the 1st row to be 0
    for i in range(0, capacity + 1):
        dp[0][i] = 0

    # initialize the 1st column to be 0
    for i in range(0, number_of_item + 1):
        dp[i][0] = 0

    for n in range(1, number_of_item + 1):
        for c in range(1, capacity + 1):
            # weight_list[0] is at the second row of dp
            # value_list[0] is at the second row of dp
            # the 1 st item in the weight_list is at index 0
            # the 1 st item in the value_list is at index 0
            # they are at the second row of dp
            if c >= weight_list[n - 1]:
                dp[n][c] = max(dp[n - 1][c], dp[n - 1][c - weight_list[n - 1]] + value_list[n - 1])
            else:
                dp[n][c] = dp[n - 1][c]
    return dp[number_of_item][capacity]


value_list = [10, 40, 30, 50]
weight_list = [5, 4, 6, 3]
number_of_item = len(weight_list)
capacity = 6
print(knapsack(value_list, weight_list, number_of_item, capacity))


# Unbounded Knapsack
def knapsack_repetition(item_value, item_weight, number_of_item, capacity):
    dp = [-1] * (number_of_item + 1)
    for c in range(1, capacity + 1):
        for n in range(0, number_of_item):
            if (c >= item_weight[n]):
                dp[c] = max(dp[c], dp[c - item_weight[n]] + item_value[n])
    return dp[capacity]
