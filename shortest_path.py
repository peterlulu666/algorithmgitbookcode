graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary
    shortest_path("A", graph_dictionary)


def shortest_path(selecting_node, graph_dictionary):
    queue = []
    visited_list = []

    # add last
    queue.append(selecting_node)

    # add last
    visited_list.append(selecting_node)

    shortest_path = {}
    shortest_path[selecting_node] = 0

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if node not in visited_list:
                # add last
                queue.append(node)
                # add last
                visited_list.append(node)
                # shortest path of the node is the shortest path of the parent node + 1
                shortest_path[node] = shortest_path[dequeue_node] + 1

    for key in graph_dictionary.keys():
        print(str(key) + ": " + str(shortest_path[key]))


main()
