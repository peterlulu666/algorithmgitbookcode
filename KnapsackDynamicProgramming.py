def knapsack(item_value, item_weight, number_of_item, capacity):
    # base case
    if number_of_item == 0 or capacity == 0:
        return 0

    # generic case

    # it will not fit into knapsack
    result = knapsack(item_value, item_weight, number_of_item - 1, capacity)
    # it will fit into knapsack
    if capacity >= item_weight[number_of_item]:
        # find the value in the above row
        # the column is in the capacity - item_weight[number_of_item])
        # add the value and item_value[number_of_item]
        result = max(result, knapsack(item_value, item_weight, number_of_item - 1,
                                      capacity - item_weight[number_of_item]) + item_value[number_of_item])
    return result


def knapsack_top_down(item_value, item_weight, number_of_item, capacity, memoized):
    # base case
    if number_of_item == 0 or capacity == 0:
        return 0
    if memoized[number_of_item - 1][capacity - 1] != -1:
        return memoized[number_of_item - 1][capacity - 1]

    # generic case

    # do not take it
    result = knapsack(item_value, item_weight, number_of_item - 1, capacity)
    # take it
    if capacity >= item_weight[number_of_item]:
        # at this point, we will take the i th item
        # so number of item decreased by 1, capacity will decrease
        # the value in the knapsack will increase, compare it with the previous value, find the greatest value
        result = max(result, knapsack(item_value, item_weight, number_of_item - 1,
                                      capacity - item_weight[number_of_item]) + item_value[number_of_item])
    memoized[number_of_item - 1][capacity - 1] = result
    return result


def knapsack_top_down_dict(item_value, item_weight, number_of_item, capacity, memoized_dict):
    # base case
    if number_of_item == 0 or capacity == 0:
        return 0
    if (number_of_item in memoized_dict) and (capacity in memoized_dict[number_of_item]):
        return memoized_dict[number_of_item][capacity]

    # generic case

    # it is greater than capacity
    # so do not take it
    result = knapsack(item_value, item_weight, number_of_item - 1, capacity)
    # it is smaller than capacity
    # so take it
    if capacity >= item_weight[number_of_item]:
        # at this point, we will take the i th item
        # so number of item decreased by 1, capacity will decrease
        # the value in the knapsack will increase, compare it with the previous value, find the greatest value
        result = max(result, knapsack(item_value, item_weight, number_of_item - 1,
                                      capacity - item_weight[number_of_item]) + item_value[number_of_item])
    memoized_dict[number_of_item] = {capacity: result}
    return result


def knapsack_botton_up(item_value, item_weight, number_of_item, capacity):
    memoized = [[-1 for i in range(capacity + 1)] for j in range(number_of_item + 1)]
    # initialize first row to 0
    for c in range(0, capacity + 1):
        memoized[0][c] = 0
    # initialize first column to 0
    for n in range(0, number_of_item + 1):
        memoized[n][0] = 0

    # in row 1
    # if the item will fit into the knapsack
    # compare value in the above row same column with value in the above row c - item_weight[n - 1] column
    # find the maximum value
    # repeat for row 2
    # repeat for row 3
    for n in range(1, number_of_item + 1):
        for c in range(1, capacity + 1):
            memoized[n][c] = memoized[n - 1][c]
            if (c >= item_weight[n - 1]):
                memoized[n][c] = max(memoized[n][c], memoized[n - 1][c - item_weight[n - 1]] + item_value[n - 1])
    return memoized[number_of_item][capacity]


# Unbounded Knapsack
def knapsack_repetition(item_value, item_weight, number_of_item, capacity):
    dp = [-1] * (number_of_item + 1)
    for c in range(1, capacity + 1):
        for n in range(0, number_of_item):
            if (c >= item_weight[n]):
                dp[c] = max(dp[c], dp[c - item_weight[n]] + item_value[n])
    return dp[capacity]


# item_value = {1: 1, 2: 2, 3: 3}
# item_weight = {1: 1, 2: 2, 3: 3}
# print(knapsack(item_value, item_weight, 3, 2))

# # python initialize 2d array
# a = [[-1] * 3] * 4
# b = [[-1 for i in range(3)] for j in range(4)]

# item_value = {1: 1, 2: 2, 3: 3}
# item_weight = {1: 1, 2: 2, 3: 3}
# capacity = 2
# number_of_item = 3
# memoized = [[-1 for i in range(capacity)] for j in range(number_of_item)]
# print(knapsack_top_down(item_value, item_weight, 3, 2, memoized))

# item_value = {1: 1, 2: 2, 3: 3}
# item_weight = {1: 1, 2: 2, 3: 3}
# memoized_dict = {}
# print(knapsack_top_down_dict(item_value, item_weight, 3, 2, memoized_dict))

# item_value = {0: 1, 1: 2, 2: 3}
# item_weight = {0: 1, 1: 2, 2: 3}
# print(knapsack_botton_up(item_value, item_weight, 3, 2))

item_value = {0: 1, 1: 2, 2: 3}
item_weight = {0: 1, 1: 2, 2: 3}
print(knapsack_repetition(item_value, item_weight, 3, 2))
