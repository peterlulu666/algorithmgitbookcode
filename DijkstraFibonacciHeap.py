import heapq

graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def Dijkstra(selecting_node):
    shortest_distance = {}
    # let all nodes' distance to be infinity
    for node in graph:
        shortest_distance[node] = infinity
    # let the selecting node's distance to be 0
    shortest_distance[selecting_node] = 0

    # Put tuple pair into the priority queue
    unvisited_queue = [(shortest_distance[node], node) for node in graph]
    heapq.heapify(unvisited_queue)

    # store visited node
    visited = []

    # let parent to be null
    parent = {}
    for node in graph:
        parent[node] = None

    # while there exists node in the unvisited_queue
    while len(unvisited_queue):
        # Pops the vertex with the smallest distance
        # (0, 'a')
        min_node_pair = heapq.heappop(unvisited_queue)
        # a
        min_node = min_node_pair[1]

        # append visited node to visited
        visited.append(min_node)

        # update shortest_distance
        # update parent
        adjacent_node_dict = graph[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if child_node not in visited:
                if shortest_distance[min_node] + weight < shortest_distance[child_node]:
                    shortest_distance[child_node] = shortest_distance[min_node] + weight
                    parent[child_node] = min_node

        # Pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)

        # Put all vertices not visited into the queue
        unvisited_queue = [(shortest_distance[node], node) for node in graph if node not in visited]
        heapq.heapify(unvisited_queue)

    print(parent)
    print(shortest_distance)


Dijkstra("a")
