def coinChange_bottom_up(coins, money):
    # in order to store solution for 0 cents, we have to reserve the additional space
    memoized = [-1] * (money + 1)
    for coin in coins:
        if coin <= money:
            memoized[coin] = 1
    for m in range(1, money + 1):
        for coin in coins:
            if coin > m:
                continue
            if memoized[m - coin] == -1:
                continue
            if memoized[m] == -1 or memoized[m - coin] + 1 < memoized[m]:
                memoized[m] = memoized[m - coin] + 1
    return memoized[money]


def coinChange(number):
    memoized = [-1] * (number + 1)
    for n in range(0, number + 1):
        memoized[n] = number - n
    for n in range(number, 0, -1):
        if n >= 25:
            memoized[int(n - 25)] = min(memoized[n] + 1, memoized[int(n - 25)])
        if n >= 10:
            memoized[int(n - 10)] = min(memoized[n] + 1, memoized[int(n - 10)])
        if n >= 5:
            memoized[int(n - 5)] = min(memoized[n] + 1, memoized[int(n - 5)])
        if n >= 1:
            memoized[int(n - 1)] = min(memoized[n] + 1, memoized[int(n - 1)])
    return memoized[0]


# coins = [25, 10, 5, 1]
# # fewest number of coin
# print(coinChange_bottom_up(coins, 3))

print(coinChange(23))

