def ways_coin_change(denomination_list, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination)]
    # initialize the 1st row
    for m in range(0, money + 1):
        if m < denomination_list[0]:
            dp[0][m] = 0
        else:
            if m % denomination_list[0] == 0:
                dp[0][m] = 1
            else:
                dp[0][m] = 0
    # initial the 1st column to be 1
    for d in range(0, number_of_denomination):
        dp[d][0] = 1

    for d in range(1, number_of_denomination):
        for m in range(1, money + 1):
            # denomination_list is starting at the first row of the dp
            # so it is the denomination_list[d]
            if m >= denomination_list[d]:
                dp[d][m] = dp[d - 1][m] + dp[d][m - denomination_list[d]]
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination - 1][money]


denomination_list = [1, 2]
number_of_denomination = len(denomination_list)
money = 5
print(ways_coin_change(denomination_list, number_of_denomination, money))


def ways_of_coin_change(denomination_list, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination + 1)]
    # initial the 1st column to be 1
    for d in range(0, number_of_denomination + 1):
        dp[d][0] = 1

    for d in range(1, number_of_denomination + 1):
        for m in range(1, money + 1):
            # denomination_list is starting at the second row of the dp
            # so it is the denomination_list[d - 1]
            if m >= denomination_list[d - 1]:
                dp[d][m] = dp[d - 1][m] + dp[d][m - denomination_list[d - 1]]
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination][money]


denomination_list = [1, 2]
number_of_denomination = len(denomination_list)
money = 5
print(ways_of_coin_change(denomination_list, number_of_denomination, money))
