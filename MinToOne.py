def minToOne(number):
    memoized = [-1] * (number + 1)
    for n in range(0, number + 1):
        memoized[n] = number - n
    for n in range(number, 0, -1):
        if n >= 1:
            memoized[int(n - 1)] = min(memoized[n] + 1, memoized[int(n - 1)])
        if n % 2 == 0:
            memoized[int(n / 2)] = min(memoized[n] + 1, memoized[int(n / 2)])
        if n % 3 == 0:
            memoized[int(n / 3)] = min(memoized[n] + 1, memoized[int(n / 3)])
    return memoized[1]


print(minToOne(100))
