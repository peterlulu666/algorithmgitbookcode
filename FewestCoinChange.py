# 1 D array coin change
def fewest_coin_change(denomination, number_of_denomination, money):
    dp = [-1] * (money + 1)
    dp[0] = 0

    for m in range(1, money + 1):
        fewest = float("inf")
        for d in range(1, number_of_denomination):
            if m >= denomination[d]:
                if 1 + dp[m - denomination[d]] < fewest:
                    fewest = 1 + dp[m - denomination[d]]
            dp[m] = fewest
    return dp[money]


denomination = [1, 5, 6, 8]
number_of_denomination = len(denomination)
money = 11
print(fewest_coin_change(denomination, number_of_denomination, money))


# 2 D array coin change
def coin_change(denomination, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination)]

    # initialize the 1st column to be 0
    for i in range(0, number_of_denomination):
        dp[i][0] = 0
    # initialize the 1st row to be 0 - money
    for i in range(0, money + 1):
        dp[0][i] = i

    for d in range(1, number_of_denomination):
        for m in range(1, money + 1):
            if m >= denomination[d]:
                dp[d][m] = min(dp[d - 1][m], dp[d][m - denomination[d]] + 1)
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination - 1][money]


denomination = [1, 5, 6, 8]
number_of_denomination = len(denomination)
money = 11
print(coin_change(denomination, number_of_denomination, money))
