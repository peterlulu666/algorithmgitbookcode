graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E"],
                    "C": ["A", "F"],
                    "D": ["B"],
                    "E": ["B"],
                    "F": ["C"]}

color = {}
for key in graph_dictionary.keys():
    color[key] = "white"

shortest_path = {}


def main():
    global graph_dictionary

    print(BFS("A", graph_dictionary))

    for key in graph_dictionary.keys():
        print(str(key) + ": " + str(shortest_path[key]))


def BFS(selecting_node, graph_dictionary):
    global color
    global shortest_path

    queue = []

    # add last
    queue.append(selecting_node)

    color[selecting_node] = "grey"

    shortest_path[selecting_node] = 0

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if color[node] == "white":
                # add last
                queue.append(node)

                color[node] = "grey"

                shortest_path[node] = shortest_path[dequeue_node] + 1

            elif color[node] == color[dequeue_node]:
                return False

        color[dequeue_node] = "black"
        print(dequeue_node)

    return True


main()
