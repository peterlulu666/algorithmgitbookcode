# graph = {"a": {"b": 10, "c": 3},
#          "b": {"c": 1, "d": 2},
#          "c": {"b": 4, "d": 8, "e": 2},
#          "d": {"e": 7},
#          "e": {"d": 9}}


graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def dijkstra(selecting_node):
    shortest_distance = {}
    unvisited = graph
    # let all nodes' distance to be infinity
    for node in unvisited:
        shortest_distance[node] = infinity
    # let the selecting node's distance to be 0
    shortest_distance[selecting_node] = 0

    # while there exists node in the unvisited
    while unvisited:
        # find the node with the smallest distance
        # min_node = None
        # for node in unvisited:
        #     # in the first iteration, we will randomly select one node to be the min_node
        #     # then we will compare it with the other node to find the node with the smallest distance
        #     if min_node is None:
        #         min_node = node
        #     elif shortest_distance[node] < shortest_distance[min_node]:
        #         min_node = node

        # # store the key value pair as distance : node
        # compare_distance = {}
        # for node in unvisited:
        #     compare_distance[shortest_distance[node]] = node
        # min_distance = (min(compare_distance))
        # min_node = compare_distance.get(min_distance)

        # # sort dictionary by value
        # compare_distance = {}
        # for node in unvisited:
        #     compare_distance[node] = shortest_distance[node]
        # compare_distance_sort = {key: value for key, value in
        #                          sorted(compare_distance.items(), key=lambda item: item[1], reverse=False)}
        # min_node = list(compare_distance_sort.keys())[0]

        # find the key with minimum value
        compare_distance = {}
        for node in unvisited:
            compare_distance[node] = shortest_distance[node]
        min_node = min(compare_distance, key=compare_distance.get)
        # min_node = [key for key in compare_distance if compare_distance[key] == min(compare_distance.values())][0]

        # compare min_node + weight with child_node's distance
        # if shortest_distance[min_node] + weight is smaller,
        # update child_node's distance
        adjacent_node_dict = unvisited[min_node]
        for child_node, weight in adjacent_node_dict.items():
            shortest_distance[child_node] = min(shortest_distance[min_node] + weight, shortest_distance[child_node])

            # add the min_node to parent_dict[child_node]

            # if shortest_distance[min_node] + weight < shortest_distance[child_node]:
            #     shortest_distance[child_node] = shortest_distance[min_node] + weight

        # pop the min_node
        unvisited.pop(min_node)

    # use while loop to
    # insert the target node to the path list at index 0
    # insert the parent node of the target node to the path list at index 0
    # insert all their parent node at index 0
    # insert the source node at index 0

    # print the path list

    # print the shortest_distance
    print(shortest_distance)


dijkstra("a")
