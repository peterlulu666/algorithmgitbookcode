graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def prim(selecting_node):
    # store the minimum edge weight in keys
    keys = {}
    # store the parent node in parent
    parent = {}
    # store graph to unvisited
    unvisited = graph
    # let key to be infinity
    for node in unvisited:
        keys[node] = infinity
    # let parent to be null
    for node in unvisited:
        parent[node] = None
    # let the selecting node's key to be 0
    keys[selecting_node] = 0

    # while there exists node in the unvisited
    while unvisited:
        # find the node with the smallest key
        compare_key = {}
        for node in unvisited:
            compare_key[node] = keys[node]
        min_node = min(compare_key, key=compare_key.get)

        # update parent
        # update keys
        adjacent_node_dict = unvisited[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if (child_node in unvisited) and (weight < keys[child_node]):
                parent[child_node] = min_node
                keys[child_node] = weight

        # pop the min_node
        unvisited.pop(min_node)

    # print the edge weight
    print(keys)
    # print the tree
    for key, value in parent.items():
        print(str(value) + " -- " + str(key) + " edge weight is " + str(keys[key]))


prim("a")
