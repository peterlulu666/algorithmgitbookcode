import heapq

graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def prim(selecting_node):
    keys = {}
    # let all nodes' keys to be infinity
    for node in graph:
        keys[node] = infinity
    # let the selecting node's key to be 0
    keys[selecting_node] = 0

    # Put tuple pair into the priority queue
    unvisited_queue = [(keys[node], node) for node in graph]
    heapq.heapify(unvisited_queue)

    # store visited node
    visited = []

    # let parent to be null
    parent = {}
    for node in graph:
        parent[node] = None

    # while there exists node in the unvisited_queue
    while len(unvisited_queue):
        # Pops a vertex with the smallest key
        # (0, 'a')
        min_node_pair = heapq.heappop(unvisited_queue)
        # a
        min_node = min_node_pair[1]

        # append visited node to visited
        visited.append(min_node)

        # update keys
        # update parent
        adjacent_node_dict = graph[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if (child_node not in visited) and (weight < keys[child_node]):
                parent[child_node] = min_node
                keys[child_node] = weight

        # in order to find the next smallest key, we will rebuild the priority queue
        # pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)

        # put all vertices not visited into the queue
        unvisited_queue = [(keys[node], node) for node in graph if node not in visited]
        heapq.heapify(unvisited_queue)

    # print the edge weight
    print(keys)
    # print the tree
    for key, value in parent.items():
        print(str(value) + " -- " + str(key) + " edge weight is " + str(keys[key]))


prim("a")
