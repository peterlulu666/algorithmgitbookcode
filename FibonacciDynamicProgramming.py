def fib(n):
    if n == 1 or n == 2:
        result = 1
    else:
        result = fib(n - 1) + fib(n - 2)
    return result


def fibMemoized(n, memoized):
    if n in memoized:
        return memoized[n]
    if n == 1 or n == 2:
        result = 1
    else:
        result = fibMemoized(n - 1, memoized) + fibMemoized(n - 2, memoized)
    memoized[n] = result
    return result


def fib_botton_up(n):
    if n == 1 or n == 2:
        return 1
    fib = {}
    # base case
    fib[1] = 1
    fib[2] = 1
    # generic case
    for i in range(3, n + 1):
        fib[i] = fib[i - 1] + fib[i - 2]
    return fib[n]


# print(fib(10))

# memoized = {}
# print(fibMemoized(10, memoized))

print(fib_botton_up(10))
